#!/bin/bash
#A script to intall numlockx on a Debian with LightDM
#To be run as superuser

apt install numlockx #Install Numlockx
cd /usr/share/lightdm/lightdm.conf.d #Go into the configuration folder
cp 01_debian.conf 01_debian.conf.bak #Backup the configuration file
echo "greeter-setup-script=/usr/bin/numlockx on" >> 01_debian.conf #Configure Numlockx
